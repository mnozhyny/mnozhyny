import { type ДвійнийДієзнак } from "../../ast/ast.interface";

export class TypeError extends Error {
  constructor(message: string, parent?: TypeError) {
    if (parent != null) super([parent.message, message].join(" -> "));
    else super(message);
  }
}

export function toFnType(змінник: Type, тіло: Type) {
  return new ArrayType([змінник, тіло], new IntType(2, 2));
}

export abstract class Type {
  constructor(protected name: string) {}

  abstract спитРівності(otherType: Type): void;
  чиРівне(otherType: Type) {
    try {
      this.спитРівності(otherType);
      return true;
    } catch (e) {
      return false;
    }
  }

  abstract спитПідвиду(subType: Type): void;
  чиПідвид(subType: Type) {
    try {
      this.спитПідвиду(subType);
      return true;
    } catch (e) {
      return false;
    }
  }

  isArray(): this is ArrayType {
    return this instanceof ArrayType;
  }

  isSet(): this is SetType {
    return this instanceof SetType;
  }

  isInt(): this is IntType {
    return this instanceof IntType;
  }

  toText() {
    return this.name;
  }

  static handleErrors(fn: () => void, parent?: TypeError) {
    try {
      fn();
    } catch (err) {
      if (err instanceof Error) {
        const tErr = new TypeError(err.message, parent);
        // TODO: handle stack
        // tErr.originalStack = err.stack
        throw tErr;
      } else {
        throw err;
      }
    }
  }

  static зВидуТаМислення(вид: Type, розмір: Type, точки: Type[]): Type {
    if (вид instanceof SetType) {
      if (точки.length > 1) {
        throw new Error(
          'Мислення виду "множина" можна використати тільки на розмір та одну точку'
        );
      }
      return SetType.зМислення(вид, розмір, точки[0]);
    }

    if (вид instanceof ArrayType) {
      return ArrayType.зМислення(вид, розмір, точки);
    }

    if (вид instanceof IntType) {
      return IntType.зМислення(вид, розмір);
    }

    throw new Error(`Вид "${вид.toText()}" не підтримує мислення`);
  }

  static зДієзнаку(вид1: Type, вид2: Type, дієзнак: ДвійнийДієзнак["дієзнак"]) {
    if (вид1 instanceof IntType) return IntType.зДієзнаку(вид1, вид2, дієзнак);
    if (вид1 instanceof ArrayType)
      return ArrayType.зДієзнаку(вид1, вид2, дієзнак);

    throw new Error(
      `Дієзнак ${дієзнак} не підтримується між видом ${вид1.toText()} та ${вид2.toText()}}`
    );
  }
}

abstract class BaseType extends Type {
  isBaseType = true;
  constructor(name?: string) {
    super(name ?? "точка");
  }
}

export class IntType extends BaseType {
  constructor(
    private readonly from: number = -Infinity,
    private readonly to: number = +Infinity
  ) {
    super("ціле");
  }

  відрізок() {
    return { from: this.from, to: this.to };
  }

  спитРівності(otherType: Type) {
    if (otherType instanceof IntType) {
      if (this.from !== otherType.from) {
        throw new TypeError(
          `Початки ("${this.from}", "${otherType.from}") обох множин цілих повинні співпадати`
        );
      }
      if ((this.to ?? Infinity) !== (otherType.to ?? Infinity)) {
        throw new TypeError(
          `Кінці ("${this.to}", "${otherType.to}") обох множин цілих повинні співпадати`
        );
      }
    } else {
      throw new TypeError(
        `Види "${this.name}" та "${otherType.toText()}" не порівнюються`
      );
    }
  }

  спитПідвиду(subType: Type) {
    if (subType instanceof IntType) {
      if (this.from > subType.from) {
        throw new TypeError(
          `Початок підвиду цілого "${subType.from}" повинен бути більшим/рівними спитувального "${this.from}"`
        );
      }
      if (this.to < subType.to) {
        throw new TypeError(
          `Кінець підвиду цілого "${subType.to}" повинен бути меншим/рівними спитувального "${this.to}"`
        );
      }
    } else if (subType instanceof Багатовид) {
      subType.спитНадвиду(this);
    } else {
      throw new TypeError(
        `Вид "${subType.toText()}" не може бути підвидом "${this.toText()}"`
      );
    }
  }

  static зМислення(вид: IntType, розмір: Type): Type {
    // TODO: звузити до додатнього цілого
    if (!(розмір instanceof IntType)) {
      throw new Error("Мислення розміру має повертати додатнє ціле");
    }

    // TODO: можливо не перезаписувати мислення, а якось обєднювати
    return new IntType(розмір.from, розмір.to);
  }

  static зДієзнаку(
    вид1: IntType,
    вид2: Type,
    дієзнак: ДвійнийДієзнак["дієзнак"]
  ) {
    if (вид2 instanceof IntType) {
      // TODO: перевірити правильність всіх дій (незабути про відємні числа)
      if (дієзнак === "+") {
        return new IntType(вид1.from + вид2.from, вид1.to + вид2.to);
      } else if (дієзнак === "-") {
        return new IntType(вид1.from - вид2.from, вид1.to - вид2.to);
      } else if (дієзнак === "*") {
        return new IntType(вид1.from * вид2.from, вид1.to * вид2.to);
      } else if (дієзнак === "/") {
        return new IntType(вид1.from / вид2.from, вид1.to / вид2.to);
      } else if (дієзнак === "%") {
        return new IntType(0, Math.abs(вид2.to));
      } else if (дієзнак === "**") {
        return new IntType(вид1.from ** вид2.from, вид1.to ** вид2.to);
      } else if (дієзнак === "..") {
        return new ArrayType(
          [new IntType()], // TODO: можливо додати сильніший вид
          new IntType(вид1.from, вид2.to)
        );
      } else if (дієзнак === "...") {
        return new ArrayType(
          [new IntType()], // TODO: можливо додати сильніший вид
          new IntType(вид1.from, вид2.to - 1)
        );
      } else if (["==", "!=", "<=", ">=", "<", ">"].includes(дієзнак)) {
        return new Багатовид([
          new IntType(), // TODO: можливо додати сильніший вид
          new NullType(),
        ]);
      }
    }

    throw new Error(`Дієзнак ${дієзнак} не підтримується для цілих`);
  }
}

// TODO: можливо зробити цей тип рівним пустій множині
export class NullType extends BaseType {
  constructor() {
    super("пустота");
  }

  спитРівності(otherType: Type) {
    if (!(otherType instanceof NullType)) {
      throw new TypeError(
        `Види "${this.name}" та "${otherType.toText()}" не порівнюються`
      );
    }
  }

  спитПідвиду(subType: Type) {
    if (!(subType instanceof NullType)) {
      throw new TypeError(
        `Вид "${subType.toText()}" не може бути підвидом "${this.toText()}"`
      );
    }
  }
}

export class SetType extends Type {
  constructor(
    private readonly elementType: Type = new IntType(),
    private readonly sizeType: Type = new IntType(1)
  ) {
    // TODO: звузити до додатнього цілого
    if (!(sizeType instanceof IntType)) {
      throw new Error("Мислення розміру має повертати додатнє ціле");
    }

    super("множина");
  }

  getType() {
    return this.elementType;
  }

  спитРівності(type: Type) {
    if (!(type instanceof SetType)) {
      throw new TypeError(
        `Види "${this.name}" та "${type.toText()}" не порівнюються`
      );
    }

    Type.handleErrors(() => {
      this.elementType.спитРівності(type.elementType);
      this.sizeType.спитРівності(type.sizeType);
    }, new TypeError("Множини не рівні"));
  }

  спитПідвиду(subType: Type) {
    if (!(subType instanceof SetType)) {
      throw new TypeError(
        `Види "${this.name}" та "${subType.toText()}" не порівнюються`
      );
    }

    this.sizeType.спитПідвиду(subType.sizeType);
    this.elementType.спитПідвиду(subType.elementType);
  }

  static зМислення(вид: SetType, розмір?: Type, точка?: Type): Type {
    // TODO: можливо не перезаписувати мислення, а якось обєднювати
    return new SetType(точка ?? вид.elementType, розмір ?? вид.sizeType);
  }
}

export class ArrayType extends Type {
  private readonly sizeType: IntType;
  private readonly elementTypes: Type[];

  constructor(
    elementTypes: Type[] = [new IntType()],
    sizeType: Type = new IntType(0, Infinity)
  ) {
    if (elementTypes.length <= 0) {
      throw new Error("Лад повиннен визначити хоча б вид першої точки");
    }
    // TODO: звузити до додатнього цілого
    if (!(sizeType instanceof IntType)) {
      throw new Error("Мислення розміру має повертати додатнє ціле");
    }

    super("лад");

    this.sizeType = sizeType;
    this.elementTypes = elementTypes;
  }

  getType(position: number) {
    if (position < 0) throw new Error("Лади не мають відємних місць");

    const element = this.elementTypes[position] as Type | undefined;
    if (element !== undefined) return element;
    else return this.elementTypes.at(-1) as Type;
  }

  спитРівності(type: Type) {
    if (!(type instanceof ArrayType)) {
      throw new TypeError(
        `Види "${this.name}" та "${type.toText()}" не порівнюються`
      );
    }

    this.спитРівностіРозміру(type.sizeType);

    for (
      let i = 0;
      i < Math.max(this.elementTypes.length, type.elementTypes.length);
      i++
    ) {
      Type.handleErrors(() => {
        this.getType(i).спитРівності(type.getType(i));
      }, new TypeError(`Види на місці ${i} в ладах не рівні`));
    }
  }

  спитРівностіРозміру(sizeType: IntType) {
    Type.handleErrors(() => {
      this.sizeType.спитРівності(sizeType);
    }, new TypeError("Види розміру в ладах не рівні"));
  }

  спитПідвиду(subType: Type) {
    if (!(subType instanceof ArrayType)) {
      throw new TypeError(
        `Види "${this.name}" та "${subType.toText()}" не порівнюються`
      );
    }

    this.спитПідвидРозміру(subType.sizeType);

    for (
      let i = 0;
      i < Math.max(this.elementTypes.length, subType.elementTypes.length);
      i++
    ) {
      Type.handleErrors(() => {
        this.getType(i).спитПідвиду(subType.getType(i));
      }, new TypeError(`Вид на місці ${i} в ладах не є підвидом спитувального на цьому місці`));
    }
  }

  спитПідвидРозміру(sizeType: IntType) {
    Type.handleErrors(() => {
      this.sizeType.спитПідвиду(sizeType);
    }, new TypeError("Розмір не є підвидом розміру спитувального"));
  }

  static зМислення(вид: ArrayType, розмір?: Type, точки?: Type[]): Type {
    // TODO: можливо не перезаписувати мислення, а якось обєднювати
    return new ArrayType(точки ?? вид.elementTypes, розмір ?? вид.sizeType);
  }

  static зДієзнаку(
    вид1: ArrayType,
    вид2: Type,
    дієзнак: ДвійнийДієзнак["дієзнак"]
  ) {
    if (вид2 instanceof IntType) {
      if (дієзнак === ".") {
        const { from, to } = вид2.відрізок();
        const можливіВиди = Array.from(
          { length: to - from + 1 },
          (x, i) => i + from
        ).map((i) => вид1.getType(i));

        if (можливіВиди.length === 1) return можливіВиди[0];
        else return new Багатовид(можливіВиди);
      }
    }

    throw new Error(`Дієзнак ${дієзнак} не підтримується для ладів`);
  }
}

export class Багатовид extends Type {
  constructor(private readonly elementTypes: Type[]) {
    super("багатовид");

    if (elementTypes.length <= 0) {
      throw new Error("Багатовид повинен містити хоча б один вид");
    }
  }

  спитРівності(otherType: Type) {
    const elementTypes =
      otherType instanceof Багатовид ? otherType.elementTypes : [otherType];

    Type.handleErrors(() => {
      elementTypes.forEach((oType) => {
        const isSomeEqual = this.elementTypes.some((type) =>
          type.чиРівне(oType)
        );
        if (!isSomeEqual) {
          throw new TypeError(
            "Вирази в багатовиразі не співпадають виразам спитувального"
          );
        }
      });
      this.elementTypes.forEach((type) => {
        const isSomeEqual = elementTypes.some((oType) => oType.чиРівне(type));
        if (!isSomeEqual) {
          throw new TypeError(
            "Вирази в багатовиразі не співпадають виразам спитувального"
          );
        }
      });
    }, new TypeError("Багатовирази не є рівними"));
  }

  спитПідвиду(subType: Type) {
    const elementTypes =
      subType instanceof Багатовид ? subType.elementTypes : [subType];

    Type.handleErrors(() => {
      elementTypes.forEach((oType) => {
        const isSomeSubType = this.elementTypes.some((type) =>
          type.чиПідвид(oType)
        );
        if (!isSomeSubType) {
          throw new TypeError(
            "Вираз в багатовиразі не є підвидом жодного виразу спитувального"
          );
        }
      });
    }, new TypeError("Багатовираз не є підвидом спитувального"));
  }

  спитНадвиду(upType: Type) {
    const elementTypes =
      upType instanceof Багатовид ? upType.elementTypes : [upType];

    Type.handleErrors(() => {
      elementTypes.forEach((oType) => {
        const isEveryUptype = this.elementTypes.every((type) =>
          oType.чиПідвид(type)
        );
        if (!isEveryUptype) {
          throw new TypeError(
            "Хоча б один вираз в багатовиразі не є підвидом спитувальному"
          );
        }
      });
    }, new TypeError("Багатовираз не є надвидом спитувального"));
  }
}
