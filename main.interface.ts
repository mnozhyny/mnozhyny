import { type Type } from "./validator/types/main";

export type СховокЗмін = Record<string, Type>;
export type СховокВидів = Record<string, Type>;
export interface Сховок {
  змінні: СховокЗмін;
  види: СховокВидів;
}

export interface Свідоцтво {
  сховок: Сховок;
  вид: Type;
  видиВислідів?: Type[];
}
