import {
  type БагатовиразЗмін,
  type Багатовираз,
  type Виклик,
  type Вираз,
  type ВиразЗмін,
  type Дія,
  type ДвійнийДієзнак,
  type Лад,
  type Множина,
  type ПрисвоєнняЗмінної,
  type Тезяр,
  type Ціле,
  type ГілкаЯкщо,
  type ОбігПо,
  type ГілкаВислід,
} from "../ast/ast.interface";
import { розібратиСтрічкуЦілого } from "../parser/expression.parser";
import {
  укластиJSГілку,
  укластиПомічніДії,
  укластиПустоту,
  НАЗВИ_ПОМІЧНИХ_ДІЙ,
} from "./utils.function";

export function buildJsAST(ast: Вираз) {
  try {
    return {
      type: "Program",
      body: [...укластиПомічніДії(), укластиВираз(ast)].filter((в) => в),
      sourceType: "module",
    };
  } catch (err: any) {
    err.message = `Спорудник: ${String(err.message)}`;
    throw err;
  }
}

function укластиВираз(вираз: Вираз) {
  if (вираз.видВиразу === "Вираз") {
    if (вираз.вираз === "Багатовираз") {
      return укластиБагатовираз(вираз as Багатовираз);
    }
  }

  if (вираз.видВиразу === "ВиразЗмін") {
    return укластиВиразЗмін(вираз as ВиразЗмін);
  }
  if (вираз.видВиразу === "ВиразВиду") return null;
  throw new Error(`Невідомий вираз "${String(вираз.видВиразу)}"`);
}

function укластиВиразЗмін(
  вираз: ВиразЗмін,
  можеМатиВислід: boolean = false
): object {
  if (вираз.вираз === "БагатовиразЗмін") {
    return укластиБагатовиразЗмін(вираз as БагатовиразЗмін);
  }
  if (вираз.вираз === "Множина") return укластиМножину(вираз as Множина);
  if (вираз.вираз === "Лад") return укластиЛад(вираз as Лад);
  if (вираз.вираз === "Ціле") return укластиЦіле(вираз as Ціле);
  if (вираз.вираз === "Тезяр") return укластиТезяр(вираз as Тезяр);
  if (вираз.вираз === "ПрисвоєнняЗмінної") {
    return укластиПрисвоєнняЗмінної(вираз as ПрисвоєнняЗмінної);
  }
  if (вираз.вираз === "ДвійнийДієзнак") {
    return укластиДвійнийДієзнак(вираз as ДвійнийДієзнак);
  }
  if (вираз.вираз === "Дія") return укластиДію(вираз as Дія);
  if (вираз.вираз === "Виклик") return укластиВиклик(вираз as Виклик);
  if (вираз.вираз === "ГілкаЯкщо") return укластиГілкуЯкщо(вираз as ГілкаЯкщо);
  if (вираз.вираз === "ГілкаВислід") {
    return укластиГілкуВислід(вираз as ГілкаВислід, можеМатиВислід);
  }
  if (вираз.вираз === "ОбігПо") return укластиОбігПо(вираз as ОбігПо);
  if (вираз.вираз === "Пустота") return укластиПустоту();
  throw new Error(`Невідомий вираз "${String(вираз.вираз)}"`);
}

function укластиБагатовираз(багатовираз: Багатовираз) {
  const виразиЗмін = багатовираз.тіло.filter(
    (в) => в.видВиразу === "ВиразЗмін"
  ) as ВиразЗмін[];

  return {
    type: "CallExpression",
    arguments: [],
    optional: false,
    callee: {
      type: "ArrowFunctionExpression",
      id: null,
      expression: true,
      generator: false,
      async: false,
      params: [],
      body: {
        type: "BlockStatement",
        body: [
          ...виразиЗмін.slice(0, -1).map((в) => укластиВиразЗмін(в, true)),
          {
            type: "ReturnStatement",
            argument: укластиВиразЗмін(виразиЗмін[виразиЗмін.length - 1]),
          },
        ],
      },
    },
  };
}

function укластиБагатовиразЗмін(багатовираз: БагатовиразЗмін) {
  const виразиЗмін = багатовираз.тіло;

  return {
    type: "CallExpression",
    arguments: [],
    optional: false,
    callee: {
      type: "ArrowFunctionExpression",
      id: null,
      expression: true,
      generator: false,
      async: false,
      params: [],
      body: {
        type: "BlockStatement",
        body: [
          ...виразиЗмін.slice(0, -1).map((в) => укластиВиразЗмін(в, true)),
          {
            type: "ReturnStatement",
            argument: укластиВиразЗмін(виразиЗмін[виразиЗмін.length - 1]),
          },
        ],
      },
    },
  };
}

function укластиМножину(множина: Множина) {
  return {
    type: "NewExpression",
    callee: {
      type: "Identifier",
      name: "Set",
    },
    arguments: [
      {
        type: "ArrayExpression",
        elements: множина.тіло.map((в) => укластиВиразЗмін(в)),
      },
    ],
  };
}

function укластиЛад(лад: Лад) {
  return {
    type: "ArrayExpression",
    elements: лад.тіло.map((в) => укластиВиразЗмін(в)),
  };
}

function укластиЦіле(ціле: Ціле) {
  return {
    type: "Literal",
    value: розібратиСтрічкуЦілого(ціле.ціле),
    raw: String(розібратиСтрічкуЦілого(ціле.ціле)),
  };
}

function укластиДвійнийДієзнак(дієзнак: ДвійнийДієзнак) {
  if (дієзнак.дієзнак === "/") {
    return {
      type: "BinaryExpression",
      left: {
        type: "BinaryExpression",
        left: укластиВиразЗмін(дієзнак.зліва),
        operator: "-",
        right: {
          type: "BinaryExpression",
          left: укластиВиразЗмін(дієзнак.зліва),
          operator: "%",
          right: укластиВиразЗмін(дієзнак.зправа),
        },
      },
      operator: дієзнак.дієзнак,
      right: укластиВиразЗмін(дієзнак.зправа),
    };
  }

  if (["==", "!=", "<=", ">=", "<", ">"].includes(дієзнак.дієзнак)) {
    return {
      type: "CallExpression",
      optional: false,
      arguments: [
        {
          type: "BinaryExpression",
          left: укластиВиразЗмін(дієзнак.зліва),
          operator:
            дієзнак.дієзнак === "=="
              ? "==="
              : дієзнак.дієзнак === "!="
              ? "!=="
              : дієзнак.дієзнак,
          right: укластиВиразЗмін(дієзнак.зправа),
        },
      ],
      callee: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ПОРІВНЯННЯ,
      },
    };
  }

  if (["."].includes(дієзнак.дієзнак)) {
    return {
      type: "MemberExpression",
      object: укластиВиразЗмін(дієзнак.зліва),
      property: укластиВиразЗмін(дієзнак.зправа),
      computed: true,
      optional: false,
    };
  }

  if ([".."].includes(дієзнак.дієзнак)) {
    return {
      type: "CallExpression",
      optional: false,
      arguments: [
        укластиВиразЗмін(дієзнак.зліва),
        укластиВиразЗмін(дієзнак.зправа),
        {
          type: "Literal",
          value: true,
          raw: "true",
        },
      ],
      callee: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ВІДРІЗОК,
      },
    };
  }

  if (["..."].includes(дієзнак.дієзнак)) {
    return {
      type: "CallExpression",
      optional: false,
      arguments: [
        укластиВиразЗмін(дієзнак.зліва),
        укластиВиразЗмін(дієзнак.зправа),
        {
          type: "Literal",
          value: false,
          raw: "false",
        },
      ],
      callee: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ВІДРІЗОК,
      },
    };
  }

  return {
    type: "BinaryExpression",
    left: укластиВиразЗмін(дієзнак.зліва),
    operator: дієзнак.дієзнак,
    right: укластиВиразЗмін(дієзнак.зправа),
  };
}

function укластиДію(дія: Дія) {
  return {
    type: "ArrowFunctionExpression",
    id: null,
    expression: true,
    generator: false,
    async: false,
    params: [укластиТезяр(дія.змінник.тезяр)], // TODO: розібратися з пустотою
    body: укластиВираз(дія.тіло),
  };
}

function укластиВиклик(виклик: Виклик) {
  return {
    type: "CallExpression",
    optional: false,
    arguments: [укластиВиразЗмін(виклик.змінник)],
    callee: {
      type: "CallExpression",
      optional: false,
      arguments: [укластиТезяр(виклик.тезяр)],
      callee: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ВИКЛИК,
      },
    },
  };
}

function укластиГілкуЯкщо(гілкаЯкщо: ГілкаЯкщо) {
  return укластиJSГілку(
    укластиВиразЗмін(гілкаЯкщо.спит),
    укластиВиразЗмін(гілкаЯкщо.гілкаТо),
    гілкаЯкщо.гілкаІнакше != null
      ? укластиВиразЗмін(гілкаЯкщо.гілкаІнакше)
      : укластиПустоту(),
    "ConditionalExpression"
  );
}

function укластиГілкуВислід(
  гілкаВислід: ГілкаВислід,
  можеМатиВислід: boolean = false
): object {
  if (можеМатиВислід) {
    return укластиJSГілку(
      укластиВиразЗмін(гілкаВислід.спит),
      {
        type: "ReturnStatement",
        argument: укластиВиразЗмін(гілкаВислід.гілкаВислід),
      },
      гілкаВислід.гілкаІнакше != null
        ? укластиВиразЗмін(гілкаВислід.гілкаІнакше)
        : укластиПустоту(),
      "IfStatement"
    );
  } else {
    return укластиJSГілку(
      укластиВиразЗмін(гілкаВислід.спит),
      укластиВиразЗмін(гілкаВислід.гілкаВислід),
      гілкаВислід.гілкаІнакше != null
        ? укластиВиразЗмін(гілкаВислід.гілкаІнакше)
        : укластиПустоту(),
      "ConditionalExpression"
    );
  }
}

function укластиОбігПо(обігПо: ОбігПо) {
  return {
    type: "ExpressionStatement",
    expression: {
      type: "CallExpression",
      callee: {
        type: "MemberExpression",
        object: укластиВиразЗмін(обігПо.обіговець),
        property: {
          type: "Identifier",
          name: "forEach",
        },
        computed: false,
        optional: false,
      },
      arguments: [укластиВиразЗмін(обігПо.дія)],
      optional: false,
    },
  };
}

function укластиТезяр(тезяр: Тезяр) {
  if (тезяр.тезяр === "тища") {
    // TODO: temporary
    return {
      type: "Identifier",
      name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ТИЩА,
    };
  }

  if (тезяр.тезяр === "огляд") {
    // TODO: temporary
    return {
      type: "MemberExpression",
      object: {
        type: "Identifier",
        name: "console",
      },
      property: {
        type: "Identifier",
        name: "log",
      },
      computed: false,
      optional: false,
    };
  }

  return {
    type: "Identifier",
    name: тезяр.тезяр,
  };
}

function укластиПрисвоєнняЗмінної(присвоєнняЗмінної: ПрисвоєнняЗмінної) {
  // TODO: присвоєння має повертати null
  return {
    type: "AssignmentExpression",
    operator: "=",
    left: укластиТезяр(присвоєнняЗмінної.змінна.тезяр),
    right: укластиВиразЗмін(присвоєнняЗмінної.значення),
  };

  // return {
  //   "type": "VariableDeclaration",
  //   "declarations": [
  //     {
  //       "type": "VariableDeclarator",
  //       "id": укластиТезяр(присвоєнняЗмінної.змінна.тезяр),
  //       "init": укластиВиразЗмін(присвоєнняЗмінної.значення)
  //     }
  //   ],
  //   "kind": "const"
  // }

  // return {
  //   "type": "CallExpression",
  //   "optional": false,
  //   "callee": {
  //     "type": "MemberExpression",
  //     "object": {
  //       "type": "Identifier",
  //       "name": "Reflect"
  //     },
  //     "property": {
  //       "type": "Identifier",
  //       "name": "defineProperty"
  //     },
  //     "computed": false,
  //     "optional": false
  //   },
  //   "arguments": [
  //     {
  //       "type": "ThisExpression", // TODO: можливо знайти краще місце для зберігання
  //     },
  //     {
  //       "type": "Literal",
  //       "value": присвоєнняЗмінної.змінна.тезяр,
  //       "raw": `'${присвоєнняЗмінної.змінна.тезяр.тезяр}'`
  //     },
  //     {
  //       "type": "ObjectExpression",
  //       "properties": [
  //         {
  //           "type": "Property",
  //           "method": false,
  //           "shorthand": false,
  //           "computed": false,
  //           "key": {
  //             "type": "Identifier",
  //             "name": "value"
  //           },
  //           "value": укластиВиразЗмін(присвоєнняЗмінної.значення),
  //           "kind": "init"
  //         },
  //         {
  //           "type": "Property",
  //           "method": false,
  //           "shorthand": false,
  //           "computed": false,
  //           "key": {
  //             "type": "Identifier",
  //             "name": "writable"
  //           },
  //           "value": {
  //             "type": "Literal",
  //             "value": false,
  //             "raw": "false"
  //           },
  //           "kind": "init"
  //         }
  //       ]
  //     }
  //   ],
  // }
}
