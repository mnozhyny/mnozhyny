export enum НАЗВИ_ПОМІЧНИХ_ДІЙ {
  ВИКЛИК = "____ВИКЛИК",
  ПОРІВНЯННЯ = "____ПОРІВНЯННЯ",
  ТИЩА = "____ТИЩА",
  ВІДРІЗОК = "____ВІДРІЗОК",
}

export function укластиПустоту() {
  return {
    type: "Literal",
    value: null,
    raw: "null",
  };
}

export function укластиJSГілку(
  спит: object,
  гілкаТо: object,
  гілкаІнакше: object,
  вид: "ConditionalExpression" | "IfStatement"
) {
  return {
    type: вид,
    test: {
      type: "BinaryExpression",
      left: спит,
      operator: "!==",
      right: укластиПустоту(),
    },
    consequent: гілкаТо,
    alternate: гілкаІнакше,
  };
}

export function укластиПомічніДії() {
  return [
    {
      type: "FunctionDeclaration",
      id: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ВИКЛИК,
      },
      expression: false,
      generator: false,
      async: false,
      params: [
        {
          type: "Identifier",
          name: "a",
        },
      ],
      body: {
        type: "BlockStatement",
        body: [
          {
            type: "ReturnStatement",
            argument: {
              type: "ConditionalExpression",
              test: {
                type: "CallExpression",
                callee: {
                  type: "MemberExpression",
                  object: {
                    type: "Identifier",
                    name: "Array",
                  },
                  property: {
                    type: "Identifier",
                    name: "isArray",
                  },
                  computed: false,
                  optional: false,
                },
                arguments: [
                  {
                    type: "Identifier",
                    name: "a",
                  },
                ],
                optional: false,
              },
              consequent: {
                type: "ArrowFunctionExpression",
                id: null,
                expression: true,
                generator: false,
                async: false,
                params: [],
                body: {
                  type: "MemberExpression",
                  object: {
                    type: "Identifier",
                    name: "a",
                  },
                  property: {
                    type: "Literal",
                    value: 1,
                    raw: "1",
                  },
                  computed: true,
                  optional: false,
                },
              },
              alternate: {
                type: "Identifier",
                name: "a",
              },
            },
          },
        ],
      },
    },
    {
      type: "FunctionDeclaration",
      id: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ПОРІВНЯННЯ,
      },
      expression: false,
      generator: false,
      async: false,
      params: [
        {
          type: "Identifier",
          name: "a",
        },
      ],
      body: {
        type: "BlockStatement",
        body: [
          {
            type: "ReturnStatement",
            argument: {
              type: "ConditionalExpression",
              test: {
                type: "Identifier",
                name: "a",
              },
              consequent: {
                type: "Identifier",
                name: "a",
              },
              alternate: укластиПустоту(),
            },
          },
        ],
      },
    },
    {
      type: "FunctionDeclaration",
      id: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ТИЩА,
      },
      expression: false,
      generator: false,
      async: false,
      params: [
        {
          type: "Identifier",
          name: "a",
        },
      ],
      body: {
        type: "BlockStatement",
        body: [
          {
            type: "ReturnStatement",
            argument: {
              type: "CallExpression",
              optional: false,
              arguments: [
                {
                  type: "CallExpression",
                  optional: false,
                  arguments: [
                    {
                      type: "SpreadElement",
                      argument: {
                        type: "Identifier",
                        name: "a",
                      },
                    },
                  ],
                  callee: {
                    type: "MemberExpression",
                    object: {
                      type: "Identifier",
                      name: "String",
                    },
                    property: {
                      type: "Identifier",
                      name: "fromCharCode",
                    },
                    computed: false,
                    optional: false,
                  },
                },
              ],
              callee: {
                type: "MemberExpression",
                object: {
                  type: "Identifier",
                  name: "console",
                },
                property: {
                  type: "Identifier",
                  name: "log",
                },
                computed: false,
                optional: false,
              },
            },
          },
        ],
      },
    },
    {
      type: "FunctionDeclaration",
      id: {
        type: "Identifier",
        name: НАЗВИ_ПОМІЧНИХ_ДІЙ.ВІДРІЗОК,
      },
      expression: false,
      generator: false,
      async: false,
      params: [
        {
          type: "Identifier",
          name: "from",
        },
        {
          type: "Identifier",
          name: "to",
        },
        {
          type: "Identifier",
          name: "inclusive",
        },
      ],
      body: {
        type: "BlockStatement",
        body: [
          {
            type: "VariableDeclaration",
            declarations: [
              {
                type: "VariableDeclarator",
                id: {
                  type: "Identifier",
                  name: "a",
                },
                init: {
                  type: "ConditionalExpression",
                  test: {
                    type: "Identifier",
                    name: "inclusive",
                  },
                  consequent: {
                    type: "Literal",
                    value: 1,
                    raw: "1",
                  },
                  alternate: {
                    type: "Literal",
                    value: 0,
                    raw: "0",
                  },
                },
              },
            ],
            kind: "const",
          },
          {
            type: "ReturnStatement",
            argument: {
              type: "CallExpression",
              callee: {
                type: "MemberExpression",
                object: {
                  type: "Identifier",
                  name: "Array",
                },
                property: {
                  type: "Identifier",
                  name: "from",
                },
                computed: false,
                optional: false,
              },
              arguments: [
                {
                  type: "ObjectExpression",
                  properties: [
                    {
                      type: "Property",
                      method: false,
                      shorthand: false,
                      computed: false,
                      key: {
                        type: "Identifier",
                        name: "length",
                      },
                      value: {
                        type: "BinaryExpression",
                        left: {
                          type: "BinaryExpression",
                          left: {
                            type: "Identifier",
                            name: "to",
                          },
                          operator: "-",
                          right: {
                            type: "Identifier",
                            name: "from",
                          },
                        },
                        operator: "+",
                        right: {
                          type: "Identifier",
                          name: "a",
                        },
                      },
                      kind: "init",
                    },
                  ],
                },
                {
                  type: "ArrowFunctionExpression",
                  id: null,
                  expression: true,
                  generator: false,
                  async: false,
                  params: [
                    {
                      type: "Identifier",
                      name: "x",
                    },
                    {
                      type: "Identifier",
                      name: "i",
                    },
                  ],
                  body: {
                    type: "BinaryExpression",
                    left: {
                      type: "Identifier",
                      name: "i",
                    },
                    operator: "+",
                    right: {
                      type: "Identifier",
                      name: "from",
                    },
                  },
                },
              ],
              optional: false,
            },
          },
        ],
      },
    },
  ];
}
