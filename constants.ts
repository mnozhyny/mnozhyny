import { type ДвійнийДієзнак } from "./ast/ast.interface";
import { type Сховок } from "./main.interface";
import {
  ArrayType,
  IntType,
  NullType,
  SetType,
  toFnType,
  Багатовид,
} from "./validator/types/main";

export const defaultVars = {
  empty: ["п", "пустота"],
  // 'вішня': 'console',
  // 'тища': 'log', // temp
};

// порядок важливий
const двійніДієзнаки: Array<ДвійнийДієзнак["дієзнак"]> = [
  "+",
  "-",

  "/",
  "%",
  "*",

  "==",
  "!=",
  "<=",
  ">=",
  "<",
  ">",

  "**",

  "...",
  "..",
  ".",
];
export const operators = {
  гілкаВислідСпит: "якщо",
  гілкаВислідТо: "вислід",
  гілкаЯкщоСпит: "якщо",
  гілкаЯкщоТо: "то",
  гілкаЯкщоІнакше: "інакше",
  обігПоПоч: "по",
  обігПоДія: "дія",
  typeAssignment: "вид",
  assignment: "є", // ше може =
  function: "в",
  functionReturn: "вислід",
  двійніДієзнаки,
  access: ".",
  expressionSeparator: ",",
  setOpen: "{",
  setClosed: "}",
  arrayOpen: "[",
  arrayClosed: "]",
  subCodeOpen: "(",
  subCodeClosed: ")",
  lineComment: "//",
  мисленняПоч: "[",
  мисленняКін: "]",
  відрізок: "...",
  stringQuote: "'",
  stringQuoteEscapeChar: "\\",
};

export const parserEscStr = "_____";
export const tabSpaceCount = 2;

export const defaultStore: Сховок = {
  змінні: {
    пустота: new NullType(),
    п: new NullType(), // TODO: можливо зробити щоб змінна посилаласяна інший
    тища: toFnType(new Багатовид([new ArrayType()]), new NullType()), // TODO: temporary
    огляд: toFnType(
      new Багатовид([new IntType(), new ArrayType(), new NullType()]),
      new NullType()
    ), // TODO: temporary
  },
  види: {
    пустота: new NullType(),
    п: new NullType(), // TODO: зробити щоб вид посилався на інший
    ціле: new IntType(),
    ц: new IntType(), // TODO: зробити щоб вид посилався на інший
    множина: new SetType(),
    м: new SetType(), // TODO: зробити щоб вид посилався на інший
    лад: new ArrayType(),
    л: new ArrayType(), // TODO: зробити щоб вид посилався на інший
  },
};
