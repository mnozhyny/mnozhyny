import { type Багатовираз, type Вираз, type Місце } from "../ast/ast.interface";
import { operators } from "../constants";
import {
  inlineCode,
  parseEmbeddedParts,
  populateFirstLevelEmbeddedParts,
} from "./embedded.parser";
import { чиДієзнакБагатовиразу } from "./expression.parser";
import { посунутиМісце } from "./location";
import { розібратиВиразВиду } from "./type.parser";
import { вПустоту, розібратиВиразЗмін } from "./var.parser";

export function розібратиВираз(
  str: string,
  refs: string[],
  місце: Місце
): Вираз {
  if (чиДієзнакБагатовиразу(str.trim())) {
    const вираз = populateFirstLevelEmbeddedParts(str, refs);
    return розібратиБагатовираз(вираз, refs, місце);
  }

  if (
    str.includes(operators.assignment) &&
    str.trim().startsWith(operators.typeAssignment)
  ) {
    return розібратиВиразВиду(
      str.slice(operators.typeAssignment.length),
      refs,
      місце
    );
  } else {
    return розібратиВиразЗмін(str, refs, місце);
  }
}

function розібратиБагатовираз(
  str: string,
  refs: string[],
  місце: Місце
): Багатовираз {
  const вирази: Вираз[] = [];

  if (str.includes(operators.expressionSeparator)) {
    let currentPosition = місце.поч;
    const виразиСтр = str.split(operators.expressionSeparator);
    for (const [index, виразСтр] of виразиСтр.entries()) {
      if (index === виразиСтр.length - 1) {
        if (виразСтр.trim().startsWith(operators.functionReturn)) {
          вирази.push(
            розібратиВираз(
              виразСтр.trim().substring(operators.functionReturn.length),
              refs,
              посунутиМісце(місце, currentPosition)
            )
          );
        } else {
          вирази.push(
            розібратиВираз(
              виразСтр,
              refs,
              посунутиМісце(місце, currentPosition)
            ),
            вПустоту(місце)
          );
        }
      } else {
        const вираз = розібратиВираз(
          виразСтр,
          refs,
          посунутиМісце(місце, currentPosition)
        );
        вирази.push(вираз);
        currentPosition += вираз.місце.кін;
      }
    }
  } else {
    вирази.push(розібратиВираз(str, refs, місце));
  }

  return вБагатовираз(вирази, місце);
}

function вБагатовираз(тіло: Вираз[], місце: Місце): Багатовираз {
  return { вираз: "Багатовираз", видВиразу: "Вираз", місце, тіло };
}

export function sourceCodeToAST(str: string) {
  try {
    const inlinedCode = inlineCode(str);
    const res = parseEmbeddedParts(inlinedCode);
    return розібратиБагатовираз(res[0], res, { поч: 0, кін: str.length });
  } catch (err: any) {
    err.message = `Розбирач: ${String(err.message)}`;

    throw err;
  }
}

export { inlineCode } from "./embedded.parser";
