import { type Місце } from "../ast/ast.interface";

export function посунутиМісце(місце: Місце, зсув: number) {
  return { поч: місце.поч + зсув, кін: місце.кін };
}
