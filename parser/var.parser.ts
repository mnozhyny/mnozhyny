import {
  type ОбігПо,
  type ГілкаЯкщо,
  type БагатовиразЗмін,
  type Виклик,
  type ВиразВиду,
  type ВиразЗмін,
  type Дія,
  type ДвійнийДієзнак,
  type Змінна,
  type Змінник,
  type Лад,
  type Місце,
  type Множина,
  type ПрисвоєнняЗмінної,
  type Пустота,
  type Тезяр,
  type Ціле,
  type Вираз,
  type ГілкаВислід,
} from "../ast/ast.interface";
import { operators } from "../constants";
import { populateFirstLevelEmbeddedParts } from "./embedded.parser";
import {
  чиТезяр,
  розділВдвоєПо,
  чиЦіле,
  чиПустота,
  чиДієзнакМножини,
  розділВдвоєПоСлову,
  чиДієзнакБагатовиразу,
  чиДієзнакЛаду,
  чиВиклик,
  розбірПоСтрічкам,
  чиГілкаВислід,
  чиГілкаЯкщо,
  чиДієзнакСтрічки,
} from "./expression.parser";
import { посунутиМісце } from "./location";
import { розібратиВираз } from "./main";
import { розібратиВиразВиду } from "./type.parser";

function вЗміннy(тезяр: Тезяр, місце: Місце, вид?: ВиразВиду): Змінна {
  return { вираз: "Змінна", видВиразу: "ВиразЗмін", місце, вид, тезяр };
}

function вЗмінник(вид: ВиразВиду, тезяр: Тезяр, місце: Місце): Змінник {
  return { вираз: "Змінник", видВиразу: "ВиразЗмін", місце, вид, тезяр };
}

function вПрисвоєнняЗмінної(
  змінна: Змінна,
  значення: ВиразЗмін,
  місце: Місце
): ПрисвоєнняЗмінної {
  return {
    вираз: "ПрисвоєнняЗмінної",
    видВиразу: "ВиразЗмін",
    місце,
    змінна,
    значення,
  };
}

function вДію(тіло: Вираз, місце: Місце, змінник: Змінник): Дія {
  return { вираз: "Дія", видВиразу: "ВиразЗмін", місце, змінник, тіло };
}

function вГілкуЯкщо(
  спит: ВиразЗмін,
  гілкаТо: ВиразЗмін,
  місце: Місце,
  гілкаІнакше?: ВиразЗмін | ГілкаЯкщо
): ГілкаЯкщо {
  return {
    вираз: "ГілкаЯкщо",
    видВиразу: "ВиразЗмін",
    місце,
    спит,
    гілкаТо,
    гілкаІнакше,
  };
}

function вГілкуВислід(
  спит: ВиразЗмін,
  гілкаВислід: ВиразЗмін,
  місце: Місце,
  гілкаІнакше?: ГілкаВислід
): ГілкаВислід {
  return {
    вираз: "ГілкаВислід",
    видВиразу: "ВиразЗмін",
    місце,
    спит,
    гілкаВислід,
    гілкаІнакше,
  };
}

function вОбігПо(обіговець: ВиразЗмін, дія: Дія, місце: Місце): ОбігПо {
  return { вираз: "ОбігПо", видВиразу: "ВиразЗмін", місце, обіговець, дія };
}

function вВиклик(тезяр: Тезяр, місце: Місце, змінник: ВиразЗмін): Виклик {
  return { вираз: "Виклик", видВиразу: "ВиразЗмін", місце, змінник, тезяр };
}

function вТезяр(тезяр: string, місце: Місце): Тезяр {
  return { вираз: "Тезяр", видВиразу: "ВиразЗмін", місце, тезяр };
}

function вЦіле(ціле: string, місце: Місце): Ціле {
  return { вираз: "Ціле", видВиразу: "ВиразЗмін", місце, ціле };
}

export function вПустоту(місце: Місце): Пустота {
  return { вираз: "Пустота", видВиразу: "ВиразЗмін", місце };
}

function вДвійнийДієзнак(
  зліва: ВиразЗмін,
  дієзнак: ДвійнийДієзнак["дієзнак"],
  зправа: ВиразЗмін,
  місце: Місце
): ДвійнийДієзнак {
  return {
    вираз: "ДвійнийДієзнак",
    видВиразу: "ВиразЗмін",
    місце,
    зліва,
    дієзнак,
    зправа,
  };
}

function вМножину(тіло: ВиразЗмін[], місце: Місце): Множина {
  return { вираз: "Множина", видВиразу: "ВиразЗмін", місце, тіло };
}

function вЛад(тіло: ВиразЗмін[], місце: Місце): Лад {
  return { вираз: "Лад", видВиразу: "ВиразЗмін", місце, тіло };
}

function вБагатовиразЗмін(тіло: ВиразЗмін[], місце: Місце): БагатовиразЗмін {
  return { вираз: "БагатовиразЗмін", видВиразу: "ВиразЗмін", місце, тіло };
}

function розібратиМножину(str: string, refs: string[], місце: Місце): Множина {
  const вирази: ВиразЗмін[] = [];

  if (str.includes(operators.expressionSeparator)) {
    let currentPosition = місце.поч;
    for (const виразСтр of str.split(operators.expressionSeparator)) {
      const вираз = розібратиВиразЗмін(
        виразСтр,
        refs,
        посунутиМісце(місце, currentPosition)
      );
      вирази.push(вираз);
      currentPosition += вираз.місце.кін;
    }
  } else {
    вирази.push(розібратиВиразЗмін(str, refs, місце));
  }

  return вМножину(вирази, місце);
}

function розібратиЛад(str: string, refs: string[], місце: Місце): Лад {
  const вирази: ВиразЗмін[] = [];

  if (str.includes(operators.expressionSeparator)) {
    let currentPosition = місце.поч;
    for (const виразСтр of str.split(operators.expressionSeparator)) {
      const вираз = розібратиВиразЗмін(
        виразСтр,
        refs,
        посунутиМісце(місце, currentPosition)
      );
      вирази.push(вираз);
      currentPosition += вираз.місце.кін;
    }
  } else {
    вирази.push(розібратиВиразЗмін(str, refs, місце));
  }

  return вЛад(вирази, місце);
}

function розібратиТезяр(str: string, місце: Місце): Тезяр {
  const тезяр = str.trim();
  if (!чиТезяр(тезяр)) {
    throw new Error(
      `Тезяр "${тезяр}" може складатися тільки з букв, чисел та _. На першому місці не може бути число чи _`
    );
  }

  return вТезяр(тезяр, місце);
}

function розібратиВиклик(str: string, refs: string[], місце: Місце): Виклик {
  const розділ = розділВдвоєПо(str.trim(), " ");
  if (розділ == null) {
    throw new Error("Виклик повиннен складатися тільки з тезяра та змінника");
  }

  return вВиклик(
    розібратиТезяр(розділ[0], місце),
    місце,
    розібратиВиразЗмін(розділ[1], refs, місце)
  );
}

function розібратиЦіле(str: string, місце: Місце): Ціле {
  const ціле = str.trim();
  if (!чиЦіле(ціле)) {
    throw new Error("Ціле може складатися тільки з чисел");
  }

  return вЦіле(ціле, місце);
}

function розібратиСтрічку(str: string, refs: string[], місце: Місце): Лад {
  return розібратиЛад(
    str
      .split("")
      .map((c) => c.charCodeAt(0))
      .join(operators.expressionSeparator),
    refs,
    місце
  );
}

function розібратиПустоту(str: string, місце: Місце): Пустота {
  const пустота = str.trim();
  if (!чиПустота(пустота)) {
    throw new Error("Вираз повиннен бути пустотою");
  }

  return вПустоту(місце);
}

function розібратиЗмінну(str: string, refs: string[], місце: Місце): Змінна {
  const змінна = str.trim();
  const розділ = розділВдвоєПо(змінна, " ");

  if (розділ != null) {
    return вЗміннy(
      розібратиТезяр(розділ[1], місце),
      місце,
      розібратиВиразВиду(розділ[0], refs, місце)
    );
  } else {
    return вЗміннy(розібратиТезяр(змінна, місце), місце);
  }
}

function розібратиЗмінник(str: string, refs: string[], місце: Місце): Змінник {
  const змінник = str.trim();

  if (
    чиДієзнакМножини(змінник) &&
    populateFirstLevelEmbeddedParts(змінник, refs).trim().length === 0
  ) {
    return вЗмінник(
      розібратиВиразВиду(
        "пустота", // TODO: винести в константи
        refs,
        місце
      ),
      вТезяр(
        "змінник", // TODO: винести в константи
        місце
      ),
      місце
    );
  }

  const розділ = розділВдвоєПо(змінник, " ");
  if (розділ == null) {
    throw new Error("Змінник повиннен має пропис виду та тезяру");
  }

  return вЗмінник(
    розібратиВиразВиду(розділ[0], refs, місце),
    розібратиТезяр(розділ[1], місце),
    місце
  );
}

function розібратиГілкуВислід(
  str: string,
  refs: string[],
  місце: Місце
): ГілкаВислід {
  const якщо = str.trim();
  const indexOfВислід = str.indexOf(operators.гілкаВислідТо);

  if (indexOfВислід < 0) {
    throw new Error(
      `Вираз гілки-висліду повиннен мати гілку "${operators.гілкаВислідТо}"`
    );
  }

  const indexOfNextЯкщо = str.indexOf(operators.гілкаВислідСпит, indexOfВислід);
  if (indexOfNextЯкщо >= 0) {
    return вГілкуВислід(
      розібратиВиразЗмін(
        якщо.substring(operators.гілкаВислідСпит.length, indexOfВислід),
        refs,
        місце
      ),
      розібратиВиразЗмін(
        якщо.substring(
          indexOfВислід + operators.гілкаВислідТо.length,
          indexOfNextЯкщо
        ),
        refs,
        місце
      ),
      місце,
      розібратиГілкуВислід(якщо.substring(indexOfNextЯкщо), refs, місце)
    );
  }

  return вГілкуВислід(
    розібратиВиразЗмін(
      якщо.substring(operators.гілкаВислідСпит.length, indexOfВислід),
      refs,
      місце
    ),
    розібратиВиразЗмін(
      якщо.substring(indexOfВислід + operators.гілкаВислідТо.length),
      refs,
      місце
    ),
    місце
  );
}

function розібратиГілкуЯкщо(
  str: string,
  refs: string[],
  місце: Місце
): ГілкаЯкщо {
  const якщо = str.trim();
  const indexOfТо = str.indexOf(operators.гілкаЯкщоТо);

  if (indexOfТо < 0) {
    throw new Error(
      `Вираз гілки-якщо повиннен мати гілку "${operators.гілкаЯкщоТо}"`
    );
  }

  const indexOfNextЯкщо = str.indexOf(operators.гілкаЯкщоСпит, indexOfТо);
  if (indexOfNextЯкщо >= 0) {
    return вГілкуЯкщо(
      розібратиВиразЗмін(
        якщо.substring(operators.гілкаЯкщоСпит.length, indexOfТо),
        refs,
        місце
      ),
      розібратиВиразЗмін(
        якщо.substring(
          indexOfТо + operators.гілкаЯкщоТо.length,
          indexOfNextЯкщо
        ),
        refs,
        місце
      ),
      місце,
      розібратиГілкуЯкщо(якщо.substring(indexOfNextЯкщо), refs, місце)
    );
  }

  const indexOfІнакше = str.indexOf(operators.гілкаЯкщоІнакше);
  if (indexOfІнакше < 0) {
    return вГілкуЯкщо(
      розібратиВиразЗмін(
        якщо.substring(operators.гілкаЯкщоСпит.length, indexOfТо),
        refs,
        місце
      ),
      розібратиВиразЗмін(
        якщо.substring(indexOfТо + operators.гілкаЯкщоТо.length),
        refs,
        місце
      ),
      місце
    );
  } else {
    return вГілкуЯкщо(
      розібратиВиразЗмін(
        якщо.substring(operators.гілкаЯкщоСпит.length, indexOfТо),
        refs,
        місце
      ),
      розібратиВиразЗмін(
        якщо.substring(indexOfТо + operators.гілкаЯкщоТо.length, indexOfІнакше),
        refs,
        місце
      ),
      місце,
      розібратиВиразЗмін(
        якщо.substring(indexOfІнакше + operators.гілкаЯкщоІнакше.length),
        refs,
        місце
      )
    );
  }
}

function розібратиОбігПо(str: string, refs: string[], місце: Місце) {
  const якщо = str.trim();
  const indexOfДія = str.indexOf(operators.обігПоДія);

  if (indexOfДія < 0) {
    throw new Error(`Вираз "${operators.обігПоДія}" повиннен мати дію`);
  }

  return вОбігПо(
    розібратиВиразЗмін(
      якщо.substring(operators.обігПоПоч.length, indexOfДія),
      refs,
      місце
    ),
    розібратиДію(
      якщо.substring(indexOfДія + operators.обігПоДія.length),
      refs,
      місце
    ),
    місце
  );
}

function розібратиДію(str: string, refs: string[], місце: Місце) {
  const розділДії = розділВдвоєПоСлову(str, operators.function);
  if (розділДії === null) {
    throw new Error("Вираз дія споруджений неправильно");
  }

  return вДію(
    розібратиВираз(розділДії[1], refs, місце),
    місце,
    розібратиЗмінник(розділДії[0], refs, місце)
  );
}

function розібратиБагатовиразЗмін(
  str: string,
  refs: string[],
  місце: Місце
): БагатовиразЗмін {
  const вирази: ВиразЗмін[] = [];

  if (str.includes(operators.expressionSeparator)) {
    let currentPosition = місце.поч;
    for (const виразСтр of str.split(operators.expressionSeparator)) {
      const вираз = розібратиВиразЗмін(
        виразСтр,
        refs,
        посунутиМісце(місце, currentPosition)
      );
      вирази.push(вираз);
      currentPosition += вираз.місце.кін;
    }
  } else {
    вирази.push(розібратиВиразЗмін(str, refs, місце));
  }

  return вБагатовиразЗмін(вирази, місце);
}

export function розібратиВиразЗмін(
  str: string,
  refs: string[],
  місце: Місце
): ВиразЗмін {
  const розділПрисвоєння = розділВдвоєПоСлову(str, operators.assignment);
  if (розділПрисвоєння != null) {
    return вПрисвоєнняЗмінної(
      розібратиЗмінну(розділПрисвоєння[0], refs, місце),
      розібратиВиразЗмін(розділПрисвоєння[1], refs, місце),
      місце
    );
  }

  if (str.trim().startsWith(operators.обігПоПоч)) {
    return розібратиОбігПо(str, refs, місце);
  }

  if (чиГілкаВислід(str.trim())) {
    return розібратиГілкуВислід(str, refs, місце);
  }

  if (чиГілкаЯкщо(str.trim())) {
    return розібратиГілкуЯкщо(str, refs, місце);
  }

  const розділДії = розділВдвоєПоСлову(str, operators.function);
  if (розділДії != null) {
    return розібратиДію(str, refs, місце);
  }

  if (чиПустота(str.trim())) {
    return розібратиПустоту(str, місце);
  }

  if (чиТезяр(str.trim())) {
    return розібратиТезяр(str, місце);
  }

  if (чиЦіле(str.trim())) {
    return розібратиЦіле(str, місце);
  }

  if (чиДієзнакБагатовиразу(str.trim())) {
    const вираз = populateFirstLevelEmbeddedParts(str, refs);
    return розібратиБагатовиразЗмін(вираз, refs, місце);
  }

  if (чиДієзнакМножини(str.trim())) {
    const вираз = populateFirstLevelEmbeddedParts(str, refs);
    return розібратиМножину(вираз, refs, місце);
  }

  if (чиДієзнакЛаду(str.trim())) {
    const вираз = populateFirstLevelEmbeddedParts(str, refs);
    return розібратиЛад(вираз, refs, місце);
  }

  if (чиДієзнакСтрічки(str.trim())) {
    const вираз = populateFirstLevelEmbeddedParts(str, refs);
    return розібратиСтрічку(вираз, refs, місце);
  }

  const розбірДвійногоДієзнаку = розбірПоСтрічкам(
    str,
    operators.двійніДієзнаки
  );
  if (розбірДвійногоДієзнаку != null) {
    return вДвійнийДієзнак(
      розібратиВиразЗмін(розбірДвійногоДієзнаку.зліва, refs, місце),
      розбірДвійногоДієзнаку.дієзнак,
      розібратиВиразЗмін(розбірДвійногоДієзнаку.зправа, refs, місце),
      місце
    );
  }

  if (чиВиклик(str.trim())) {
    return розібратиВиклик(str, refs, місце);
  }

  throw new Error(`Невідомий вираз змін "${str}"`);
}
