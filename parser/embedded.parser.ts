import { operators, parserEscStr, tabSpaceCount } from "../constants";
import { escapeRegExp } from "./expression.parser";

export function inlineCode(source: string) {
  const lineSeparater = "\n";

  const code: string[] = [];
  let subCode: string = "";

  const rows = source.split(lineSeparater);

  function processSubCode() {
    if (subCode.length > 0) {
      const currentSubCodeRows = subCode;
      subCode = "";

      const inlinedCode = inlineCode(currentSubCodeRows);
      code[
        code.length - 1
      ] += ` ${operators.subCodeOpen}${inlinedCode}${operators.subCodeClosed}`;
    }
  }

  for (const [rowIndex, row] of rows.entries()) {
    // TODO: check if this row.length statement is needed
    // if (row.length < tabSpaceCount) continue;
    const allSpaces = row.match(/^\s*/);
    const level =
      allSpaces == null ? 0 : Math.floor(allSpaces[0].length / tabSpaceCount);

    if (row === "" || row.startsWith(operators.lineComment)) continue;

    if (rowIndex === 0 && level !== 0) {
      throw new Error("Перший рядок блоку повинен починатися без відступу");
    }

    if (level === 0) {
      processSubCode();
      code.push(`${row.trim()}`);
    }

    if (level >= 1) {
      subCode += row.slice(tabSpaceCount) + lineSeparater;
    }
  }
  processSubCode();

  return `${code.join(",")}`;
}

function parseQuotes(strings: string[]): string[] {
  let res = [...strings];

  const quotes = [operators.stringQuote];
  const escapeChar = operators.stringQuoteEscapeChar;

  quotes.forEach(function (quote) {
    // create quote regex
    const qRE = new RegExp(
      [
        `(${escapeRegExp(quote)})`,
        `(?:(?=(${escapeRegExp(escapeChar)}?))\\2.)*?`,
        "\\1",
      ].join("")
    );

    let ids: number[] = [];

    function replaceToken(token: string, idx: string, str: string) {
      // save token to res
      const refId = res.push(token.slice(quote.length, -quote.length)) - 1;

      ids.push(refId);

      return parserEscStr + String(refId) + parserEscStr;
    }

    res.forEach(function (str, i) {
      let prevStr;

      // replace paren tokens till there’s none
      let a = 0;
      while (str !== prevStr) {
        prevStr = str;
        str = str.replace(qRE, replaceToken);
        if (a++ > 10e3) {
          throw Error(
            "References have circular dependency. Please, check them."
          );
        }
      }

      res[i] = str;
    });

    // wrap found refs to quotes
    ids = ids.reverse();
    res = res.map(function (str) {
      ids.forEach(function (id) {
        str = str.replace(
          new RegExp(
            "(\\" + parserEscStr + String(id) + "\\" + parserEscStr + ")",
            "g"
          ),
          quote + "$1" + quote
        );
      });
      return str;
    });
  });

  return res;
}

function parseBrackets(strings: string[]): string[] {
  let res = [...strings];

  const brackets = [
    [operators.subCodeOpen, operators.subCodeClosed],
    [operators.setOpen, operators.setClosed],
    [operators.arrayOpen, operators.arrayClosed],
  ];

  brackets.forEach(function (bracket) {
    // create parenthesis regex
    const pRE = new RegExp(
      [
        "\\",
        bracket[0],
        "[^\\",
        bracket[0],
        "\\",
        bracket[1],
        "]*\\",
        bracket[1],
      ].join("")
    );

    let ids: number[] = [];

    function replaceToken(token: string, idx: string, str: string) {
      // save token to res
      const refId =
        res.push(token.slice(bracket[0].length, -bracket[1].length)) - 1;

      ids.push(refId);

      return parserEscStr + String(refId) + parserEscStr;
    }

    res.forEach(function (str, i) {
      let prevStr;

      // replace paren tokens till there’s none
      let a = 0;
      while (str !== prevStr) {
        prevStr = str;
        str = str.replace(pRE, replaceToken);
        if (a++ > 10e3) {
          throw Error(
            "References have circular dependency. Please, check them."
          );
        }
      }

      res[i] = str;
    });

    // wrap found refs to brackets
    ids = ids.reverse();
    res = res.map(function (str) {
      ids.forEach(function (id) {
        str = str.replace(
          new RegExp(
            "(\\" + parserEscStr + String(id) + "\\" + parserEscStr + ")",
            "g"
          ),
          bracket[0] + "$1" + bracket[1]
        );
      });
      return str;
    });
  });

  return res;
}

export function parseEmbeddedParts(str: string): string[] {
  return parseBrackets(parseQuotes([str]));
}

export function populateFirstLevelEmbeddedParts(str: string, refs: string[]) {
  const parseBracketReg = new RegExp(
    "\\" + parserEscStr + "([0-9]+)" + "\\" + parserEscStr
  );

  let resStr: string = "";

  let match;
  let a = 0;
  while ((match = parseBracketReg.exec(str)) != null) {
    if (a++ > 10e3) throw Error("Circular references in parenthesis");

    resStr += str.slice(0, match.index - 1);

    resStr += refs[Number(match[1])];

    str = str.slice(match.index + match[0].length + 1);
  }

  resStr += str;
  return resStr;
}
