import {
  type ТезярВиду,
  type ВиразВиду,
  type Місце,
  type ПрисвоєнняВиду,
  type БагатовиразВиду,
  type ЦілеВид,
  type ВідрізокВид,
  type Мислення,
} from "../ast/ast.interface";
import { operators } from "../constants";
import { populateFirstLevelEmbeddedParts } from "./embedded.parser";
import {
  чиТезярВиду,
  розділВдвоєПоСлову,
  чиДієзнакБагатовиразу,
  чиЦіле,
  розділВдвоєПо,
  чиВідрізок,
  чиМислення,
} from "./expression.parser";
import { посунутиМісце } from "./location";

function вПрисвоєнняВиду(
  тезяр: ТезярВиду,
  значення: ВиразВиду,
  місце: Місце
): ПрисвоєнняВиду {
  return {
    вираз: "ПрисвоєнняВиду",
    видВиразу: "ВиразВиду",
    місце,
    тезяр,
    значення,
  };
}

function вЦілеВид(ціле: string, місце: Місце): ЦілеВид {
  return { вираз: "ЦілеВид", видВиразу: "ВиразВиду", місце, ціле };
}

function вВідрізокВид(поч: ЦілеВид, кін: ЦілеВид, місце: Місце): ВідрізокВид {
  return { вираз: "ВідрізокВид", видВиразу: "ВиразВиду", місце, поч, кін };
}

function вМислення(
  тезяр: ТезярВиду,
  розмір: ВиразВиду,
  точки: ВиразВиду[],
  місце: Місце
): Мислення {
  return {
    вираз: "Мислення",
    видВиразу: "ВиразВиду",
    місце,
    тезяр,
    розмір,
    точки,
  };
}

function вТезярВиду(тезяр: string, місце: Місце): ТезярВиду {
  return { вираз: "ТезярВиду", видВиразу: "ВиразВиду", місце, тезяр };
}

function вБагатовиразВиду(тіло: ВиразВиду[], місце: Місце): БагатовиразВиду {
  return { вираз: "БагатовиразВиду", видВиразу: "ВиразВиду", місце, тіло };
}

function розібратиЦілеВид(str: string, місце: Місце): ЦілеВид {
  const ціле = str.trim();
  if (!чиЦіле(ціле)) {
    throw new Error("Ціле може складатися тільки з чисел");
  }

  return вЦілеВид(ціле, місце);
}

function розібратиВідрізокВид(str: string, місце: Місце): ВідрізокВид {
  const відрізок = str.trim();
  const розділ = розділВдвоєПо(відрізок, operators.відрізок);

  if (розділ == null) {
    throw new Error(
      "Відрізок повиннен складатися з двох цілих і відповідного дієзнака"
    );
  }

  return вВідрізокВид(
    розібратиЦілеВид(розділ[0], місце),
    розібратиЦілеВид(розділ[1], місце),
    місце
  );
}

function розібратиМислення(
  str: string,
  refs: string[],
  місце: Місце
): Мислення {
  const мислення = str.trim();
  const indexOf = мислення.indexOf(operators.мисленняПоч);
  if (indexOf < 0) {
    throw new Error("Мислення не відповідає знаколаду");
  }

  const тезяр = мислення.substring(0, indexOf);

  const [розмір, ...точки] = populateFirstLevelEmbeddedParts(
    мислення.substring(indexOf),
    refs
  ).split(operators.expressionSeparator);

  return вМислення(
    розібратиТезярВиду(тезяр, місце),
    розібратиВиразВиду(розмір, refs, місце),
    точки.map((т) => розібратиВиразВиду(т, refs, місце)),
    місце
  );
}

function розібратиТезярВиду(str: string, місце: Місце): ТезярВиду {
  const тезярВиду = str.trim();
  if (!чиТезярВиду(тезярВиду)) {
    throw new Error(
      "Тезяр виду може складатися тільки з букв та числе. На першому місці не може бути число"
    );
  }

  return вТезярВиду(тезярВиду, місце);
}

function розібратиБагатовиразВиду(
  str: string,
  refs: string[],
  місце: Місце
): БагатовиразВиду {
  const вирази: ВиразВиду[] = [];

  if (str.includes(operators.expressionSeparator)) {
    let currentPosition = місце.поч;
    for (const виразСтр of str.split(operators.expressionSeparator)) {
      const вираз = розібратиВиразВиду(
        виразСтр,
        refs,
        посунутиМісце(місце, currentPosition)
      );
      вирази.push(вираз);
      currentPosition += вираз.місце.кін;
    }
  } else {
    вирази.push(розібратиВиразВиду(str, refs, місце));
  }

  return вБагатовиразВиду(вирази, місце);
}

export function розібратиВиразВиду(
  str: string,
  refs: string[],
  місце: Місце
): ВиразВиду {
  const розділПрисвоєння = розділВдвоєПоСлову(str, operators.assignment);

  if (розділПрисвоєння != null && чиТезярВиду(розділПрисвоєння[0].trim())) {
    const [left, right] = розділПрисвоєння;
    return вПрисвоєнняВиду(
      розібратиТезярВиду(left, місце),
      розібратиВиразВиду(right, refs, місце),
      місце
    );
  }

  if (чиДієзнакБагатовиразу(str.trim())) {
    const вираз = populateFirstLevelEmbeddedParts(str, refs);

    return розібратиБагатовиразВиду(вираз, refs, місце);
  }

  if (чиЦіле(str.trim())) {
    return розібратиЦілеВид(str, місце);
  }

  if (чиТезярВиду(str.trim())) {
    return розібратиТезярВиду(str, місце);
  }

  if (чиВідрізок(str.trim())) {
    return розібратиВідрізокВид(str, місце);
  }

  if (чиМислення(str.trim())) {
    return розібратиМислення(str, refs, місце);
  }

  throw new Error(`Невідомий вираз виду "${str}"`);
}
