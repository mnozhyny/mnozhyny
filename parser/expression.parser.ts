import { defaultVars, operators } from "../constants";

const тезярRegExp = "[А-ЩЬЮЯҐЄІЇа-щьюяґєії][0-9А-ЩЬЮЯҐЄІЇа-щьюяґєії_]*";
export const цілеНескінечне = "00";
const цілеExp = `-?(([1-9]\\d*)|0|${цілеНескінечне})`;

export function розібратиСтрічкуЦілого(стр: string) {
  if (стр === цілеНескінечне) return Infinity;
  if (стр === `-${цілеНескінечне}`) return -Infinity;

  return Number(стр);
}

export function чиТезяр(str: string) {
  return new RegExp(`^${тезярRegExp}$`).test(str);
}

export function чиТезярВиду(str: string) {
  return new RegExp(`^${тезярRegExp}$`).test(str);
}

export function чиВиклик(str: string) {
  return new RegExp(`^${тезярRegExp}\\s+.+$`).test(str);
}

export function чиЦіле(str: string) {
  return new RegExp(`^${цілеExp}$`).test(str);
}

export function чиВідрізок(str: string) {
  return new RegExp(
    `^${цілеExp}${escapeRegExp(operators.відрізок)}${цілеExp}$`
  ).test(str);
}

export function чиМислення(str: string) {
  return new RegExp(
    `^${тезярRegExp}${escapeRegExp(operators.мисленняПоч)}.*${escapeRegExp(
      operators.мисленняКін
    )}$`
  ).test(str);
}

export function чиПустота(str: string) {
  // TODO: видалити defaultVars якщо можливо
  return defaultVars.empty.some((v) => v === str);
}

export function чиДієзнакБагатовиразу(str: string) {
  // TODO: can match `(sd) (gr)`
  return (
    str.trim().startsWith(operators.subCodeOpen) &&
    str.trim().endsWith(operators.subCodeClosed)
  );
}

export function чиДієзнакЛаду(str: string) {
  // TODO: can match `(sd) (gr)`
  return (
    str.trim().startsWith(operators.arrayOpen) &&
    str.trim().endsWith(operators.arrayClosed)
  );
}

export function чиДієзнакМножини(str: string) {
  // TODO: can match `(sd) (gr)`
  return (
    str.trim().startsWith(operators.setOpen) &&
    str.trim().endsWith(operators.setClosed)
  );
}

export function чиДієзнакСтрічки(str: string) {
  // TODO: can match `(sd) (gr)`
  return (
    str.trim().startsWith(operators.stringQuote) &&
    str.trim().endsWith(operators.stringQuote)
  );
}

export function чиГілкаЯкщо(str: string) {
  return (
    str.trim().startsWith(operators.гілкаЯкщоСпит) &&
    str.trim().includes(operators.гілкаЯкщоТо)
  );
}

export function чиГілкаВислід(str: string) {
  return (
    str.trim().startsWith(operators.гілкаВислідСпит) &&
    str.trim().includes(operators.гілкаВислідТо)
  );
}

export function escapeRegExp(str: string) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

export function розділВдвоєПо(str: string, роздільник: string) {
  const index = str.indexOf(роздільник);

  if (index > 0) {
    return [str.substring(0, index), str.substring(index + роздільник.length)];
  } else {
    return null;
  }
}

export function розділВдвоєПоСлову(str: string, роздільник: string) {
  const index = str.search(new RegExp(`\\s${escapeRegExp(роздільник)}\\s`));

  if (index > 0) {
    return [
      str.substring(0, index),
      str.substring(index + роздільник.length + 2),
    ];
  } else {
    return null;
  }
}

export function розбірПоСтрічкам<P extends string>(
  str: string,
  роздільники: P[]
) {
  // порядок роздільників важливий
  for (const роздільник of роздільники) {
    const match = str.match(
      new RegExp(
        `(^.+?)(?<!${escapeRegExp(роздільник)})(${escapeRegExp(
          роздільник
        )})(?!${escapeRegExp(роздільник)})(.+?$)`
      )
    );

    if (match != null) {
      return {
        зліва: match[1],
        дієзнак: match[2] as P,
        зправа: match[3],
      };
    }
  }

  return null;
}
