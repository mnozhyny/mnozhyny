import { readFileSync, writeFileSync } from "fs";
import { basename, dirname, join } from "path";
// @ts-expect-error astring does not support types
import { generate } from "astring";
import { inlineCode, sourceCodeToAST } from "./parser/main";
import { validateAST } from "./validator/main";
import { buildJsAST } from "./builder/main";

export function укластиJS(filePath: string, outputPath?: string) {
  const outFolder = dirname(outputPath ?? filePath);
  const outFileName = basename(outputPath ?? filePath);

  const source = readFileSync(filePath, "utf8");

  writeFileSync(join(outFolder, outFileName + ".inlined"), inlineCode(source));

  const AST = sourceCodeToAST(source);
  writeFileSync(
    join(outFolder, outFileName + ".ast.json"),
    JSON.stringify(AST)
  );

  validateAST(AST);
  // console.log(свідоцтво)

  const jsAST = buildJsAST(AST);
  const js = generate(jsAST);
  writeFileSync(join(outFolder, outFileName + ".js"), js);
}

export * from "./parser/main";
export * from "./validator/main";
export * from "./builder/main";

const args = process.argv.slice(2);

if (args[0].length > 0) укластиJS(args[0], args[1]);
